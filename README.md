# HueReader

This project is a simple command line application that you can
use to poll the state of your Hue smart home system periodically
and save the results to JSON files.

It was created as part of my Data Science course at
[Heilbronn University](https://www.hs-heilbronn.de).

## Build

I don't provide any pre-built binaries.
To use the program for yourself, clone the project and build it:

If you have [installed Go](https://go.dev/doc/install), just run
`go build` in the project folder to build the binary.

To build for a specific platform, run
`env GOOS=<operating system> GOARCH=<chip architecture> go build`
instead. For example, you would run `env GOOS=linux GOARCH=arm64 go build`
if you are targeting a recent version of the Raspberry Pi.

## Usage

You need two things before using this program: 1) The IP address of your Hue bridge
and 2) a valid username for use with the bridge's API. You can find good instructions
on how to do this in the [Hue API Getting Started](https://developers.meethue.com/develop/get-started-2/).

Now, if you have built the program (see above), you are ready to go. `huereader`
takes four arguments, two of which are required:

1) [required] `-bridge_ip=<your bridge's IP address>`
2) [required] `-username=<username of you Hue brigde API user>`
3) `-duration=<how long the program will run>` (default: `336h0m0s`, which is 14 days)
4) `-interval=<how often the polling will happen>` (default: `1m0s`, one minute)

A folder named "huereader" is auto-created in your home directory
when you run the program. That's the place where all output goes.
In "huereader", two sub-directories are created: "lights" and "sensors".
Pretty self-explanatory: The data about your lights goes into "lights",
the data about your sensors (and switches, and other stuff) goes into "sensors".

The created output files are named "\<current timestamp\>.json", where
\<current timestamp\> is the current timestamp in the format "2022-01-31_15-31-10"
(January 31, 2022; 3:31:10 pm).

## Contributing

This project is not actively maintained, so no contributions will be accepted.
Feel free to fork it though! :-)

## Authors

[Florian Fichtner](mailto:lorchtner@gmail.com)

## License

This project is licensed under the MIT license. See the
[LICENSE](https://gitlab.com/lorchtner/huereader/-/blob/main/LICENSE) for the full text.
