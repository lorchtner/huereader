package main

import (
	"log"
	"os"
	"path/filepath"
	"time"
)

func main() {
	args, err1 := parseCLArgs()
	if err1 != nil {
		log.Fatalln(err1)
	}

	homeDir, err2 := os.UserHomeDir()
	if err2 != nil {
		log.Panicln("Was unable to read the user's home directory:", err2)
	}

	baseURL := "http://" + *args.bridgeIP + "/api/" + *args.username
	lightsURL := baseURL + "/lights"
	sensorsURL := baseURL + "/sensors"

	baseDir := filepath.Join(homeDir, "huereader")
	dataDir := filepath.Join(baseDir, "data")
	lightsDir := filepath.Join(dataDir, "lights")
	sensorsDir := filepath.Join(dataDir, "sensors")

	if err := mkdirs(0700, &lightsDir, &sensorsDir); err != nil {
		panic(err)
	}

	periodicExec(*args.duration, *args.interval, func(timestamp time.Time) {
		log.Println("Collecting state of Hue system...")

		filename := timestamp.Format("2006-01-02_15-04-05") + ".json"
		lightsFile := filepath.Join(lightsDir, filename)
		sensorsFile := filepath.Join(sensorsDir, filename)

		if err := restToJSONFile(&lightsURL, &lightsFile); err != nil {
			panic(err)
		}
		if err := restToJSONFile(&sensorsURL, &sensorsFile); err != nil {
			panic(err)
		}
	})

	log.Println("Collecting Hue system data finished!")
}
