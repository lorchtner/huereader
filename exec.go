package main

import "time"

// periodicExec calls a function periodically for a limited amount of time,
// blocking the current goroutine for that amount of time.
//
// The execDuration parameter specifies when to stop.
//
// The period parameter specifies how often to call the function.
func periodicExec(
	execDuration time.Duration,
	period time.Duration,
	fn func(timestamp time.Time),
) {
	doneChannel := make(chan bool)
	defer func() { doneChannel <- true }()

	ticker := time.NewTicker(period)
	defer ticker.Stop()

	go func() {
		for {
			select {
			case <-doneChannel:
				return
			case timestamp := <-ticker.C:
				fn(timestamp)
			}
		}
	}()

	time.Sleep(execDuration)
}
