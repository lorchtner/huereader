package main

import (
	"errors"
	"io"
	"io/fs"
	"net/http"
	"os"
)

func restToJSONFile(url *string, filepath *string) error {
	response, httpErr := http.Get(*url)
	if httpErr != nil {
		return httpErr
	}
	defer func(body io.ReadCloser) {
		err := body.Close()
		if err != nil {
			panic(err)
		}
	}(response.Body)
	if response.StatusCode != http.StatusOK {
		return errors.New("Response status not OK: " + response.Status)
	}

	body, responseBodyReadErr := io.ReadAll(response.Body)
	if responseBodyReadErr != nil {
		return responseBodyReadErr
	}

	fileWriteErr := os.WriteFile(*filepath, body, 0666)
	if fileWriteErr != nil {
		return fileWriteErr
	}

	return nil
}

func dirExists(dirpath *string) (bool, error) {
	if _, err := os.Stat(*dirpath); err == nil {
		return true, nil
	} else if errors.Is(err, fs.ErrNotExist) {
		return false, nil
	} else {
		return false, err
	}
}

func mkdirs(perm fs.FileMode, dirpaths ...*string) error {
	for _, dirpath := range dirpaths {
		if exists, err := dirExists(dirpath); err == nil && !exists {
			if err := os.MkdirAll(*dirpath, perm); err != nil {
				return err
			}
		} else if err != nil {
			return err
		}
	}

	return nil
}
