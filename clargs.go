package main

import (
	"errors"
	"flag"
	"time"
)

const (
	// defaultDuration specifies for how long the program should run.
	defaultDuration = 14 * 24 * time.Hour

	// defaultInterval specifies how often the Hue system status should
	// be collected.
	defaultInterval = 1 * time.Minute
)

type clargs struct {
	bridgeIP *string
	username *string
	duration *time.Duration
	interval *time.Duration
}

func parseCLArgs() (*clargs, error) {
	bridgeIPFlag := flag.String(
		"bridge_ip",
		"",
		"Hue bridge IP address",
	)
	usernameFlag := flag.String(
		"username",
		"",
		"Hue bridge API username",
	)
	durationFlag := flag.Duration(
		"duration",
		defaultDuration,
		"How long the program should run",
	)
	intervalFlag := flag.Duration(
		"interval",
		defaultInterval,
		"How often the Hue system state should be recorded",
	)

	flag.Parse()

	if *bridgeIPFlag == "" {
		return nil, missingFlagErr("bridge_ip")
	}
	if *usernameFlag == "" {
		return nil, missingFlagErr("username")
	}

	args := clargs{
		bridgeIP: bridgeIPFlag,
		username: usernameFlag,
		duration: durationFlag,
		interval: intervalFlag,
	}

	return &args, nil
}

func missingFlagErr(flagName string) error {
	return errors.New("the " + flagName + " flag was not specified")
}
